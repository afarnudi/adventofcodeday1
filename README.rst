A solution to the "day 1" problem of the "advent of code" 2017 edition.
For more info on the problem please visit: https://adventofcode.com/2017/day/1 