"""This programme solves the problem to the day 1 of the advent of code 2017:
https://adventofcode.com/2017/day/1
"""
import argparse


def read_first_line(file_name):
    """Read the first line of a text file.

    Read the first line and strip it of all extra characters.

    Parameters
    ----------
    file_name : str
        The path to the input text file.

    Returns
    -------
    str
        Text in the first line.
    """
    with open(file_name, "r") as rf:
        line = rf.readline()
        # make sure there are no extra characters at the end.
        line = line.strip()
    return line


def parse_captcha(captcha):
    """Parses an input string captcha.

    It checks for repeating consecutive numbers and sums them together.

    Parameters
    ----------
    captcha : str
        target captcha.

    Returns
    -------
    int
        solution of the captcha.
    """
    result = 0
    for i in range(len(captcha)):
        if captcha[i - 1] == captcha[i]:
            try:
                result += int(captcha[i])
            except:
                continue
    return result


def create_parser():
    """Create a parser for the AOCD1 programme.

    Returns
    -------
    argparse.ArgumentParser
        parser
    """
    parser = argparse.ArgumentParser(
        prog="AOCD1",
        description="Welcome to AOCD1. "
        "Where we present a programme to solve the first problem of the advent of code 2017 on https://adventofcode.com/2017/day/1",
    )
    parser.add_argument(
        "file_name", type=str, help="input text file containing captcha string."
    )
    return parser


def run():
    parser = create_parser()
    user_args = parser.parse_args()

    captcha = read_first_line(user_args.file_name)
    result = parse_captcha(captcha)
    print(f"The answer to the captcha in the {user_args.file_name} is:\n{result}")


if __name__ == "__main__":
    run()
