from main import parse_captcha


def test_parse_captcha_1122():
    assert parse_captcha("1122") == 3


def test_parse_captcha_91212129():
    assert parse_captcha("91212129") == 9


def test_parse_captcha_1111():
    assert parse_captcha("1111") == 4


def test_parse_captcha_1234():
    assert parse_captcha("1234") == 0

def test_parse_captcha_11234():
    assert parse_captcha("11234") == 1

def test_parse_captcha_1a1234():
    assert parse_captcha("1a1234") == 0
